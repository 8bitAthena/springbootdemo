package com.example.demo.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Specify type of object want to work with and id for type that we want. Type of id is Long
// Spec as repository because interface is responsible for data access
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    //want custom function to finde user by email. Optional is a container object
    //in SQL: SELECT * FROM student WHERE email = ?whatever we parse?
    //The below is JBSQL. ?1 is whatever we parse. Student refers to the entire class so can access all students
    @Query("SELECT s FROM Student s WHERE s.email = ?1")
    Optional<Student> findStudentByEmail (String email);
}
