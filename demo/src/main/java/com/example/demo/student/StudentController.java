package com.example.demo.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.List;
// all resources for API in here

//Serves rest endpoints (URLs)
@RestController
//assigns to localhost:8080/api/v1/student
@RequestMapping("api/v1/student")
public class StudentController {
    private final StudentService studentService;

    //means that the above student service should be instantiated and then passed into the below method ie. injected
    // to tell the compiler that the student controller is a class, need to tell it it is  spring bean
    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<Student> getStudents() {
        //will use the student service method to get students. Uses n tier design pattern
        return studentService.getStudents();
    }

    //implement api that takes payload and stores on database. POST creates new resources.
    @PostMapping
    // want to take student from client and map it to the Student in the func arg by taking it from the
    // request body
    public void registerNewStudent (@RequestBody Student student) {
        studentService.addNewStudent(student);

    }

    //parse studentID
    @DeleteMapping(path = "{studentID}")
    // grab the id and store in variable id
    // eg http://localhost:8080/api/v1/student/1, the path variable is 1.
    public void deleteStudent(@PathVariable("studentID") Long studentId) {
        studentService.deleteStudent(studentId);
    }

    //want to update email or name
    @PutMapping(path = "{studentID}")
    public void updateStudent(@PathVariable("studentID") Long studentId, @RequestParam(required = false) String email,
                              @RequestParam(required = false) String name) {
        studentService.updateStudent(studentId, email, name);
    }
}
