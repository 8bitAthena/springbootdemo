package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

// customizable parameters of a student
@Configuration
public class StudentConfig {
    // an interface used to indicate that a bean should run when it is contained within a SpringApplication
    @Bean
    CommandLineRunner commandLineRunner (StudentRepository repository) {
        return args -> {
            Student mariam = new Student(
                    "Mariam",
                    "mariamrulz@email.com",
                    LocalDate.of(2000, Month.JANUARY, 5)
            );

            Student john = new Student(
                    "John",
                    "johnrulz@email.com",
                    LocalDate.of(2004, Month.JANUARY, 5)
            );
            // to save list of students to db
            repository.saveAll(Arrays.asList(mariam, john));
        };
    }

}
