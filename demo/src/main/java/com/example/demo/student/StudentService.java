package com.example.demo.student;
//responsible for business logic at service layer, contacts data layer

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//Tells any injection points that this is a spring bean that needs to be instantiated.
//could also use @Component, but service is more specific
@Service
public class StudentService {

    //connect to data layer. Use dependency injection
    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents() {
        // returns a list to us
        return studentRepository.findAll();
    }

    //this is some business logic for if an email is duplicate or not.
    public void addNewStudent(Student student) {
        //try to find student by email
        Optional<Student> studentOptional = studentRepository
                .findStudentByEmail(student.getEmail());
        //to make sure no duplicates
        if(studentOptional.isPresent()) {
            throw new IllegalStateException("email taken");
        }
        // if student not present, add them
        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        // if student doesn't exist, throw exception
        if(!studentRepository.existsById(studentId)){
            throw new IllegalStateException("student with id " + studentId + " does not exist");
        }
        studentRepository.deleteById(studentId);
    }

    // allows you to use setters from obj instead of sql
    @Transactional
    public void updateStudent(Long studentId, String email, String name) {
        if(!studentRepository.existsById(studentId)){
            throw new IllegalStateException("student with id " + studentId + " does not exist");
        }
        Student s = studentRepository.getById(studentId);

        if (email != null && email.length() > 0 && !s.getEmail().equals(email)) {
            // also need to check email hasn't been taken
            Optional<Student> studentOptional = studentRepository
                    .findStudentByEmail(email);
            if(studentOptional.isPresent()) {
                throw new IllegalStateException("email taken");
            }
            s.setEmail(email);
        }
        if (name != null && name.length() > 0 && !s.getName().equals(name)) s.setName(name);
        studentRepository.save(s);
    }
}
