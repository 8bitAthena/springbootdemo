package com.example.demo.student;;

//if change from hibernate to other provide, everything will still work because of this package import. Follows persistent implementation
import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table
public class Student {
    // create something to generate student id's as primary keys in the table.
    // a persistent object is one that continues to exist after the program that created it has been unloaded,
    // persistence provider is a library that provides the functionality to persist objects.
    @Id
    @SequenceGenerator(
            name = "student_sequence",
            // Sequence objects are used to sequentially generate numeric values,
            // sequenceName (Optional): The name of the database sequence object from which to obtain primary key values
            sequenceName =  "student_sequence",
            // The amount to increment by when allocating sequence numbers from the sequence
            allocationSize = 1
    )
    @GeneratedValue (
            // The primary key generation strategy that the persistence provider must use
            strategy = GenerationType.SEQUENCE,
            // references the sequence gen to use
            generator = "student_sequence"
    )
    private Long id;
    private String name;
    private String email;
    private LocalDate dob;
    // don't want age stored in database as can calculate using date of birth
    // indicates that the following field has no need to be column in database
    @Transient
    private Integer age;

    public Student () {

    }

    //without id as db generates it for us
    public Student( String name, String email, LocalDate dob) {
        this.name = name;
        this.email = email;
        this.dob = dob;
    }

    public Student(Long id, String name, String email, LocalDate dob) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.dob = dob;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return Period.between(dob, LocalDate.now()).getYears();
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", dob=" + dob +
                '}';
    }
}
