package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
// This table is added to the database:
//	create table student (
//			id int8 not null,
//			age int4,
//			dob date,
//			email varchar(255),
//	name varchar(255),
//	primary key (id)
//    )
}
