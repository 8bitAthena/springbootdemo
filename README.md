# Student Management System (Backend)

### Project Description
The following project is a Springboot application. A RESTful API is implemented to facilitate a  
student management system.  

To create and alter the database, Postgres and Hibernate were used.  
Maven was selected as the build automation tool.  
Testing of request functionality was done with Postman.

A 3-Tier architecture was employed and is described by:
- API Layer
- Service Layer
- Data Access Layer

The project originated from the course offered at,  

https://amigoscode.com/courses/  

and the certificate of completion can be found in the repository root.

### The Repository

The src folder contains all necessary java files for project execution.  
The run.sh file is a shell script that can be used to start the web server.  
The certificate of completion for the Getting Started with Springboot course.

### To Run

In a terminal window execute the following command  

``$ sh run.sh``

This will run the maven lifecycle up to and including the package phase, as well as start  
an instance of the application. Multiple instances can be run by specifying the port.  
By default the application is run on port 8080,  

http://localhost:8080/

However, the path to view students currently in the database is specified by,  

http://localhost:8080/api/v1/student

